const gulp = require('gulp'),
    ugLify = require('gulp-uglify'), //压缩js  
    imageMin = require('gulp-imagemin'), //压缩图片  
    htmlMin = require('gulp-htmlmin'), //压缩html  
    changed = require('gulp-changed'), //检查改变状态  
    cleanCSS = require('gulp-clean-css');
    del = require('del'),
    pngquant = require('imagemin-pngquant'),
    babel = require("gulp-babel"),
    browserSync = require("browser-sync").create(); //浏览器实时刷新  

const workPath={
    entry:'src',
    output:'dist'
}

//删除dist下的所有文件  
gulp.task('delete', function(cb) {
    return del([workPath.output+'/*'], cb);
});

// 压缩图片  
gulp.task('images', function() {
    return gulp.src(workPath.entry+'/images/*.*')
        .pipe(imageMin({
            progressive: true, // 无损压缩JPG图片  
            svgoPlugins: [{ removeViewBox: false }], // 不移除svg的viewbox属性  
            interlaced: true,
            optimizationLevel: 5,
            multipass: true,
            accurate: true, //高精度模式
            quality: "high", //图像质量:low, medium, high and veryhigh;
            method: "smallfry", //网格优化:mpe, ssim, ms-ssim and smallfry;
            use: [pngquant()]
        }))
        .pipe(gulp.dest(workPath.output+'/images'));
});

//压缩css
gulp.task('css', () => {
    return gulp.src(workPath.entry+'/**/*.css')
    .pipe(changed(workPath.output, { hasChanged: changed.compareSha1Digest }))    
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest(workPath.output))
      .pipe(browserSync.reload({ stream: true }));      
  });


//压缩js  
gulp.task("script", function() {
    return gulp.src([workPath.entry+'/**/*.js',"!"+workPath.entry+"/plugins/**/*.js"])
        .pipe(changed(workPath.output, { hasChanged: changed.compareSha1Digest }))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(ugLify())
        .pipe(gulp.dest(workPath.output))
        .pipe(browserSync.reload({ stream: true }));
});

//压缩html  
gulp.task('html', function() {
    var options = {
        removeComments: true, //清除HTML注释  
        collapseWhitespace: true, //压缩HTML  
        removeScriptTypeAttributes: true, //删除<script>的type="text/javascript"  
        collapseBooleanAttributes: false, //省略布尔属性的值 <input checked="true"/> ==> <input />
        removeEmptyAttributes: false, //删除所有空格作属性值 <input id="" /> ==> <input />
        removeStyleLinkTypeAttributes: false,
        minifyJS: true, //压缩页面JS  
        minifyCSS: true, //压缩页面CSS  
    };

    return gulp.src(workPath.entry+'/**/*.html')
        .pipe(changed(workPath.output, { hasChanged: changed.compareSha1Digest }))            
        .pipe(htmlMin(options))
        .pipe(gulp.dest(workPath.output))
        .pipe(browserSync.reload({ stream: true }));
});

//复制第三方库文件
gulp.task('copy',  function() {
    return gulp.src(workPath.entry+'/plugins/**/*')
      .pipe(gulp.dest(workPath.output+'/plugins'));
  });

//启动热更新  
gulp.task('serve', ['delete'], function() {
    gulp.start(['images','script','css','copy','html']);
    browserSync.init({
        port: 2018,
        server: {
            baseDir: workPath.output,
            index:"./index.html",
            proxy:'192.168.19.131'
        }
    });
    gulp.watch(workPath.entry+'/**/*.js', ['script']); //监控文件变化，自动更新  
    gulp.watch(workPath.entry+'/**/*.css', ['css']);
    gulp.watch(workPath.entry+'/**/*.html', ['html']);
    gulp.watch(workPath.entry+'/images/*.*', ['images']);
});


gulp.task('default', ['serve']);